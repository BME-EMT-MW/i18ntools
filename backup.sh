#!/bin/bash
# set -x

# Author: Hanák Péter, hanak@emt.bme.hu, 2019-10-08
# Last modified 2019-10-08

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Updating Pootle's translation files from Pootle's database"
pushd ~/dev/pootle > /dev/null
source env/bin/activate
pootle sync_stores
deactivate
popd > /dev/null

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Backing up Pootle's translation folder"
TR_DIR="/var/www/pootle/env/lib/python2.7/site-packages/pootle/translations"
/bin/tar czf ~/backup/pootleTranslations-$(date +%y%m%d%H%M).tgz ${TR_DIR}

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Backing up the Pootle database, please enter the password for 'root':"
/usr/bin/mysqldump -u root -p pootle > ~/backup/pootleDatabase-$(date +%y%m%d%H%M).sql

exit 0
