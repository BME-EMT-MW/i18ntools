#!/bin/bash
# set -x

# Author: Hanák Péter, hanak@emt.bme.hu, 2019-04-22
# Last modified 2020-03-22

usage() { echo;
	  echo "Update strings for exisiting projects from templates.pot files";
	  echo "1. Update all templates in Pootle's database by projects";
	  echo "2. Update all .po files from Pootle's database by projects";
	  echo "3. Update all .po files against newer templates by projects";
	  echo "4. Update Pootle's database against .po files by projects";
	  echo
	  echo "Usage: $0 <option>";
	  echo "  where <option> is one of:";
	  echo "    -d: Don't create only delete existing .po files (be careful!)";
	  echo "    -o: Create new or overwrite existing .po files (be careful!)";
	  echo "    -k: Create new or extend existing .po files (recommended)";
	  #?? echo "    -u: Update Pootle's database with Pootle's translation files";
	  echo "    -h: Usage (this text)";
	  exit 1;
	}
UPDATE=
# Leading colon (:) supresses error messages in case of unrecognized options
while getopts ":dokuh" opt; do
    case $opt in
	d) DELETE=true; OWRITE=false; KEEP=false ;;
	o) OWRITE=true; DELETE=false; KEEP=false ;;
	k) KEEP=true; DELETE=false; OWRITE=false ;;
	#?? u) UPDATE=true; KEEP=true; DELETE=false; OWRITE=false ;;
        h) usage ;;
	\?) echo "Invalid option: -$OPTARG; try '$0 -h'" >&2; exit 1 ;;
    esac
done

if [ ${OPTIND} -ne 2 ]
then
    # if called with no options or multiple options
    usage
fi

RRD=$(dirname $(cd $(dirname $0) >/dev/null 2>&1 && pwd)) # Repo Root Directory

TGT_LANGS="de_DE en_US hu_HU sv_SE"

TRANS_DIR="/var/www/pootle/env/lib/python2.7/site-packages/pootle/translations"
POTS_DIR="${RRD}/i18ntools/pots"
TEMPLATE="templates.pot"
POT2PO="/var/www/pootle/env/bin/pot2po --progress=none"

if [ ! -d ${TRANS_DIR} ]
then
    echo "${TRANS_DIR} not found."
    exit 1
fi

pushd ~/dev/pootle > /dev/null
source env/bin/activate

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if [[ ${DELETE} != true ]]
then
    echo
    echo "--- Update 1) all templates in and 2) all .po files from";
    echo "    Pootle's database by projects";
    for i in $(find ${POTS_DIR} -mindepth 1 -type d -printf "%f\n" | sort )
    do
	pootle update_stores --project=$i --language=templates
	echo "Template in Pootle's database for project $i updated."
	pootle sync_stores --project=$i
	echo "All .po files for project $i updated."
    done
fi

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
echo "--- Update 3) all .po files in Pootle translations folder against";
echo "    newer templates, or delete them by projects";

for i in $(find ${POTS_DIR} -mindepth 1 -type d -printf "%f\n" | sort )
do
    pushd ${TRANS_DIR}/$i > /dev/null
    # echo $i
    if [ -f ${TEMPLATE} ]
    then
	for l in ${TGT_LANGS}
	do
	    if [ -f $l.po ]
	    then
		if [ ${DELETE} = true ]
		then
	    	    /bin/rm $l.po
		    echo "$i/$l.po deleted"
		elif [ ${KEEP} = true ]
		then
		    ${POT2PO} -t $l.po ${TEMPLATE} $l.po
		    echo "$i/$l.po is created or extended"
		else
		    ${POT2PO} ${TEMPLATE} $l.po
		    echo "$i/$l.po is created or overwritten"
		fi
	    else
		if [ ! ${DELETE} = true ]
		then 
		    ${POT2PO} ${TEMPLATE} $l.po
		    echo "$i/$l.po is created"
		fi
	    fi
	done
    fi
    popd > /dev/null
done

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if [[ ${UPDATE} == true && ${DELETE} != true ]]
if [[ ${DELETE} != true ]]
then
    echo
    echo "--- Update 4) Pootle's database against .po files by projects";
    for i in $(find ${POTS_DIR} -mindepth 1 -type d -printf "%f\n" | sort )
    do
	pootle update_stores --project=$i
	echo "Pootle's database against .po files for project $i updated."
    done
fi

deactivate
popd > /dev/null

exit 0
