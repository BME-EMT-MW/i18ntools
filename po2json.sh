#!/bin/bash
# set -x

# Author: Hanák Péter, hanak@emt.bme.hu, 2019-04-10
# Last modified 2020-03-18

usage() { echo;
	  echo "Optionally update Pootle's translation files from Pootle's database.";
	  echo "Then generate .json files in i18next format from .po files;";
	  echo "sources: subfolders in Pootle translations folder,";
	  echo "targets: games/i18n and webapp/www/i18n folders.";
	  echo
	  echo "Usage: $0 <option>";
	  echo "  where <option> is one of:";
	  echo "    -u: Update translation files from database, then generate files.";
	  echo "    -n: Generate files but make no update from database";
	  echo "    -h: Usage (this text)";
	  exit 1;
	}

NOUPD=true
# Leading colon (:) supresses error messages in case of unrecognized options
while getopts ":nuh" opt; do
    case $opt in
        n) NOUPD=true ;;
        u) NOUPD=false ;;
        h) usage ;;
	\?) echo "Invalid option: -$OPTARG; try '$0 -h'" >&2; exit 1 ;;
    esac
done

if [ ${OPTIND} -ne 2 ]
then
    # if called with no options or multiple options
    usage
fi

RRD=$(dirname $(cd $(dirname $0) >/dev/null 2>&1 && pwd)) # Repo Root Directory

SRC_DIR="/var/www/pootle/env/lib/python2.7/site-packages/pootle/translations"
POTS_DIR="${RRD}/i18ntools/pots"
GI18N_DIR="${RRD}/games/i18n"
WI18N_DIR="${RRD}/webapp/www/i18n"

if [ ! -d ${SRC_DIR} ]
then
    echo "${SRC_DIR} not found."
    exit 1
elif [ ! -d ${POTS_DIR} ]
then
    echo "${POTS_DIR} not found."
    exit 1
elif [ ! -d ${GI18N_DIR} ]
then
    echo "${GI18N_DIR} not found."
    exit 1
elif [ ! -d ${WI18N_DIR} ]
then
    echo "${WI18N_DIR} not found."
    exit 1
fi

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if [[ ${NOUPD} != true ]]
then
    echo "Updating Pootle's translation files from Pootle's database"
    pushd ~/dev/pootle > /dev/null
    source env/bin/activate
    pootle sync_stores
    deactivate
    popd > /dev/null
fi

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
for i in $(find ${POTS_DIR} -mindepth 1 -type d -printf "%f\n" | sort )
do
    for f in $(find ${SRC_DIR}/$i -name "*.po") 
    do 
	LANG=${f##*/}
	LANG=${LANG%.po}
	fn=$i-${LANG}.json

	export LANGUAGE=en_US.UTF-8
	export LC_ALL=en_US.UTF-8
	
	/usr/bin/i18next-conv -l ${LANG} --skipUntranslated --quiet -s $f -t ${GI18N_DIR}/$fn
	
	if [[ $? ]]
	then
	    echo "$fn is generated."
	else
	    echo "Error while processing $f."
	fi
    done

done

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Move webapp*.json files to ${WI18N_DIR}"
/bin/mv --verbose ${GI18N_DIR}/webapp*.json ${WI18N_DIR}

exit 0
