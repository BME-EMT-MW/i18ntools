# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FrontVL-MW
# This file is distributed under the same license as the frame package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: frame 0.1\n"
"Report-Msgid-Bugs-To: mw-pootle@emt.bme.hu\n"
"POT-Creation-Date: 2022-04-07 18:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#, javascript-format
msgid "Amazing, this result is in the top REPLACED% of your scores!"
msgstr ""

msgid "Excellent, this is your best score from the past REPLACED games!"
msgstr ""

msgid "Excellent. Suggestion: consider the Hard level."
msgstr ""

msgid "Good job."
msgstr ""

msgid "Great, this is your best score from the past REPLACED days!"
msgstr ""

msgid "Hurray, this is your best score from the past 24 hours!"
msgstr ""

msgid "NEW HIGHSCORE!"
msgstr ""

#, javascript-format
msgid "Nice, this result is in the top REPLACED% of your scores!"
msgstr ""

msgid "Suggestion: consider the Easy level."
msgstr ""

msgid "Suggestion: consider the Medium level."
msgstr ""

msgid "Very good. Suggestion: consider the Medium level."
msgstr ""

msgid "close"
msgstr ""

msgid "evaluate"
msgstr ""

msgid "game ended"
msgstr ""

msgid "help"
msgstr ""

msgid "pause"
msgstr ""

msgid "play again"
msgstr ""

msgid "resume"
msgstr ""

msgid "settings"
msgstr ""

msgid "start"
msgstr ""

msgid "stop"
msgstr ""

msgid "your result is"
msgstr ""
