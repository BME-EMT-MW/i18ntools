# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FrontVL-MW
# This file is distributed under the same license as the boutique package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: boutique 0.1\n"
"Report-Msgid-Bugs-To: mw-pootle@emt.bme.hu\n"
"POT-Creation-Date: 2022-04-07 18:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Be aware that sweaters of different sizes will make your task more difficult later as larger sweaters can not be put on smaller ones."
msgstr ""

msgid "Be fast and avoid unnecessary steps to get a better score."
msgstr ""

msgid "Boutique"
msgstr ""

msgid "Boutique is a logic puzzle; its goal is to properly arrange the sweaters on the shelf."
msgstr ""

msgid "Difficulty"
msgstr ""

msgid "For moving a sweater, first click on the sweater to be moved, then on the place you want to put it on."
msgstr ""

msgid "Help"
msgstr ""

msgid "Help Susie put the sweaters back into a single stack at the left side on the shelf in as few steps as possible."
msgstr ""

msgid "Here"
msgstr ""

msgid "Maybe you have to move all the sweaters several times for getting them in the right order."
msgstr ""

msgid "Put the sweaters into the left stack from the other two stacks in the order in which they are faintly shown in the left stack."
msgstr ""

msgid "She is happy to have more and more shoppers, but annoyed that they are throwing up the sweaters by the end of the day."
msgstr ""

msgid "Susie has always loved knitting, so she opened a little sweater boutique a few weeks ago."
msgstr ""

msgid "Sweaters are of different sizes; a larger sweater cannot be put on a smaller one, not even temporarily."
msgstr ""

msgid "The number of sweaters increases with the difficulty level, from three to seven."
msgstr ""

msgid "Try to rearrange the sweaters by the least number of clicks."
msgstr ""

msgid "boutique"
msgstr ""

msgid "easy"
msgstr ""

msgid "hard"
msgstr ""

msgid "medium"
msgstr ""
