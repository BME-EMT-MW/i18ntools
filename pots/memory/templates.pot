# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FrontVL-MW
# This file is distributed under the same license as the memory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: memory 0.1\n"
"Report-Msgid-Bugs-To: mw-pootle@emt.bme.hu\n"
"POT-Creation-Date: 2022-04-07 18:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Help"
msgstr ""

msgid "If they match, they disappear."
msgstr ""

msgid "In the beginning of the game, cards are laid face down."
msgstr ""

msgid "In the beginning, all cards are laid face down."
msgstr ""

msgid "In the popular Memory Game, the player has to remember the images on card faces."
msgstr ""

msgid "The fewer the number of bad hits the higher your score."
msgstr ""

msgid "The goal is remove all cards from the play area."
msgstr ""

msgid "The popular Memory Game tests the visuo-spatial working memory."
msgstr ""

msgid "Two cards can be flipped face up in each turn by clicking on them."
msgstr ""

msgid "Your task is to memorize the images on the card faces and their positions."
msgstr ""

msgid "Your task is to turn over pairs of matching cards."
msgstr ""

msgid "easy"
msgstr ""

msgid "hard"
msgstr ""

msgid "medium"
msgstr ""

msgid "memory"
msgstr ""
