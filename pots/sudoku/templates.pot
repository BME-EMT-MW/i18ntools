# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FrontVL-MW
# This file is distributed under the same license as the sudoku package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: sudoku 0.1\n"
"Report-Msgid-Bugs-To: mw-pootle@emt.bme.hu\n"
"POT-Creation-Date: 2022-04-07 18:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "(Click on it again to unselect it.)"
msgstr ""

msgid "Alternatively, unselect all digits in the top area, and then click in the cell with the left mouse button."
msgstr ""

msgid "Click on it again to unselect it."
msgstr ""

msgid "Difficulty"
msgstr ""

msgid "Each digit must appear exactly once in each row, column and block."
msgstr ""

msgid "Every row, column and block must contain the digits from 1 to 9."
msgstr ""

msgid "Fill in the empty cells with digits obeying the following rules."
msgstr ""

msgid "First, select a digit in the top area by clicking on it."
msgstr ""

msgid "First, select a digit in the top of the play area by clicking on it."
msgstr ""

msgid "Help"
msgstr ""

msgid "If you violate the placement rules the digit will turn red."
msgstr ""

msgid "In note mode, select a digit in the top area, and then click in the cell where the note has to be inserted, or erased from."
msgstr ""

msgid "In note mode, select a digit in the top of the play area, and then click in the cell where the note has to be inserted, or erased from."
msgstr ""

msgid "It is played on a grid of 3x3 blocks, with each block consisting of 3x3 cells."
msgstr ""

msgid "Note mode can be turned on and off by clicking on the note button in the bottom of the play area."
msgstr ""

msgid "Note mode can be turned on and off by clicking on the note button."
msgstr ""

msgid "Notes may be completely erased from a cell by clicking in the cell with the right mouse button."
msgstr ""

msgid "Sudoku, a logic-based number-placement puzzle, tests the executive function."
msgstr ""

msgid "Then click in an empty cell with the left mouse button to fill it, or a previously filled cell to refill it, with the digit selected."
msgstr ""

msgid "To erase a previously filled cell click in it with the right mouse button."
msgstr ""

msgid "When the game starts, some cells contain digits from 1 to 9."
msgstr ""

msgid "Write notes in the empty cells to remind yourself which digits are allowed in those cells."
msgstr ""

msgid "You can write notes in the empty cells to remind yourself which digits are allowed in those cells."
msgstr ""

msgid "Your task is to fill in the empty cells with digits obeying the following rules."
msgstr ""

msgid "easy"
msgstr ""

msgid "hard"
msgstr ""

msgid "medium"
msgstr ""

msgid "note"
msgstr ""

msgid "sudoku"
msgstr ""
