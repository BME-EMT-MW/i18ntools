# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FrontVL-MW
# This file is distributed under the same license as the differences package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: differences 0.1\n"
"Report-Msgid-Bugs-To: mw-pootle@emt.bme.hu\n"
"POT-Creation-Date: 2022-04-07 18:54+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Be fast and you score more points."
msgstr ""

msgid "By moving the pointers, locate the differences!"
msgstr ""

msgid "Differences, an observation challenging game, tests the visuo-spatial working memory."
msgstr ""

msgid "Difficulty"
msgstr ""

msgid "Drag the pointer to the spots that are different, and click with the mouse."
msgstr ""

msgid "Drag the pointers to the details that differ from each other, and click on them with the left mouse button."
msgstr ""

msgid "Help"
msgstr ""

msgid "If you make mistakes you will loose points."
msgstr ""

msgid "The goal is to quickly find the differences between the pictures."
msgstr ""

msgid "You will see two, almost identical pictures on the screen, and two hand-shaped pointers."
msgstr ""

msgid "differences"
msgstr ""

msgid "easy"
msgstr ""

msgid "hard"
msgstr ""

msgid "medium"
msgstr ""
