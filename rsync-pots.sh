#!/bin/bash
# set -x

# Author: Hanák Péter, hanak@emt.bme.hu, 2019-04-10
# Last modified 2020-03-18

usage() { echo;
	  echo "Synchronize templates.pot files; sources: subfolders in i18ntools'";
	  echo "pots folder, targets: subfolders in Pootle's translations folder.";
	  echo
	  echo "Usage: $0 <option>";
	  echo "  where <option> is one of:";
	  echo "    -d: Dry-run, i.e., trial run with no changes made (default)";
	  echo "    -e: Execute file changes and database update";
	  echo "    -h: Usage (this text)";
	  exit 1;
	}

DRYRUN="--dry-run"
# Leading colon (:) supresses error messages in case of unrecognized options
while getopts ":deh" opt; do
    case $opt in
        d) DRYRUN="--dry-run" ;;
	e) DRYRUN="" ;;
        h) usage ;;
	\?) echo "Invalid option: -$OPTARG; try '$0 -h'" >&2; exit 1 ;;
    esac
done

if [ ${OPTIND} -ne 2 ]
then
    # if called with no options or multiple options
    usage
fi

RRD=$(dirname $(cd $(dirname $0) >/dev/null 2>&1 && pwd)) # Repo Root Directory

SRC_DIR="${RRD}/i18ntools/pots"
TGT_DIR="/var/www/pootle/env/lib/python2.7/site-packages/pootle/translations"
TEMPLATE="templates.pot"

if [ ! -d ${SRC_DIR} ]
then
    echo "${SRC_DIR} not found."
    exit 1
elif [ ! -d ${TGT_DIR} ]
then
    echo "${TGT_DIR} not found."
    exit 1
fi

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Updating template files from pots to Pootle's translations folder"

if [[ ${DRYRUN} == "--dry-run" ]]
then
    echo "Dry-run with no changes made"
fi

# Important! Trailing slash matters in '${SRC_DIR}/'!
/usr/bin/rsync --recursive --verbose ${DRYRUN} \
	       --include=${TEMPLATE} ${SRC_DIR}/ ${TGT_DIR}

exit 0
