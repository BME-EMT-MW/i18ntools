# i18ntools

Gettext translation tools for MW. Use it as user 'pootle' on kgate.emt.bme.hu,
folder 'frontvl/repo/i18ntools'.

## List of bash-scripts (in recommended usage order)
--------------------
A) Prepare translations, ie. update Pootle database from MW source files:
1. **backup.sh:** backup Pootle's translations folders and database
2. **gettext-extract.sh:** update local repos, then merge or create templates.pot
   files in the local pots folder.
3. **rsync-pots.sh:** update Pootle's translations folders and database from the
   local pots folder.
4. **pot2po.sh:** create .po files from .pot files in Pootle's translations folders.

B) Perform translations, then create translations from Pootle database as .json files:
5. **po2json.sh:** update translation files from Pootle's database, then generate
   .json files.

## Getting help
------------
Run the selected script with no swith or with the '-h' switch (except backup).

## Short description and usage
---------------------------

### backup.sh

Backup Pootle's translations folders and database, store
timestamped backups in the backup folder in Pootle's home.

Database root password is in /home/hanak/pootle/dbroot.txt on kgate.

--- Restore
Restore database by (takes time):
   mysql --user root --password pootle < /home/pootle/backup/pootleDatabase-<date>.sql
Restore .po files:
   (cd /; tar xzvf /home/pootle/backup/pootleTranslations-<date>.tgz \
   --exclude=var/www/pootle/env/lib/python2.7/site-packages/pootle/translations/{terminology,tutorial})
   
### gettext-extract.sh

Update games and webapp repositories from GitLab, and then
create templates.pot files in the pots/\<dir\> directories by
extracting texts to be translated from *.js, *.ts, *.elm and
*.txt files found in the games and webapp repositories.

Usage: ./gettext-extract.sh \<option\>

  where \<option\> is one of:
*    -m: Merge with templates.pot files if exist
*    -o: Overwrite templates.pot files if exist (recommended)
*    -h: Usage (this text)

Overwrite (-o) is recommended if old translations have to be made obsolete
in Pootle.

### rsync-pots.sh

Synchronize templates.pot files; sources: subfolders in i18ntools'
pots folder, targets: subfolders in Pootle's translations folder.

Usage: ./rsync-pots.sh \<option\>

  where \<option\> is one of:
*    -d: Dry-run, i.e., trial run with no changes made
*    -e: Execute file changes, i.e. create new templates.pot files
*    -h: Usage (this text)

### pot2po.sh

Create .po files from .pot files in subfolders of Pootle's
translations folder.

Usage: ./pot2po.sh \<option\>

  where \<option\> is one of:
*    -d: Don't create only delete existing .po files (be careful!)
*    -o: Create new or overwrite existing .po files (be careful!)
*    -k: Create new or extend existing .po files (recommended)
*    -h: Usage (this text)

Be careful: overwrite (-o) deletes all existing translations from Pootle's database! Therefore, keep (-k) is recommended.

### po2json.sh

Optionally update Pootle's translation files from Pootle's database.
Then generate .json files in i18next format from .po files;
sources: subfolders in Pootle translations folder,
targets: games/i18n and webapp/www/i18n folders.

Usage: ./po2json.sh \<option\>

  where \<option\> is one of:
    
*  -u: Update translation files from database, then generate files.
*  -n: Generate files but make no update from database
*  -h: Usage (this text)
