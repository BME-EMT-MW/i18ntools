#!/bin/bash
# set -x

# Author: Hanák Péter, hanak@emt.bme.hu, 2019-04-06
# Last modified 2020-03-22

usage() { echo;
	  echo "Update games and webapp repositories from GitLab, and then";
	  echo "create templates.pot files in the pots/<dir> directories by";
	  echo "extracting texts to be translated from *.js, *.ts, *.elm and";
	  echo "*.txt files found in the games and webapp repositories.";
	  echo
	  echo "Usage: $0 <option>";
	  echo "  where <option> is one of:";
	  echo "    -m: Merge with templates.pot files if exist";
	  echo "    -o: Overwrite templates.pot files if exist (recommended)";
	  echo "    -h: Usage (this text)";
	  exit 1;
	}

MERGE=false
# Leading colon (:) supresses error messages in case of unrecognized options
while getopts ":moh" opt; do
    case $opt in
        m) MERGE=true ;;
        o) MERGE=false ;;
        h) usage ;;
	\?) echo "Invalid option: -$OPTARG; try '$0 -h'" >&2; exit 1 ;;
    esac
done

if [ ${OPTIND} -ne 2 ]
then
    # if called with no options or multiple options
    usage
fi

RRD=$(dirname $(cd $(dirname $0) >/dev/null 2>&1 && pwd)) # Repo Root Directory

POTS_DIR="${RRD}/i18ntools/pots"

GAMES_ROOT="${RRD}/games"
WEBAPP_ROOT="${RRD}/webapp"
GAMES_PATH="${GAMES_ROOT}/games"
FRAME_PATH="${GAMES_ROOT}/frame"
LIBS_PATH="${GAMES_ROOT}/libs"
WEBAPP_PATH="${WEBAPP_ROOT}/bin"
STATS_PATH="${WEBAPP_ROOT}/www"

TEMPLATE="templates.pot"
OPT_OUTPUT_FILE="--output ${TEMPLATE}"
OPT_OUTPUT_DETAILS="--no-location --sort-output --no-wrap --copyright-holder=FrontVL-MW" # --strict
OPT_REPORT_ADDRESS="--msgid-bugs-address=mw-pootle@emt.bme.hu"

OPT_INPUT_LANG="--language=JavaScript --from-code=UTF-8"
OPT_LANG_SPECIFIC="--keyword=__ --keyword=_n:1,2"
OPT_ALL="--extract-all"

VERSION="0.1"

if [ ! -d ${GAMES_ROOT} ]
then
    echo ${GAMES_ROOT} not found.
    exit 1
elif [ ! -d ${WEBAPP_ROOT} ]
then
    echo ${WEBAPP_ROOT} not found.
    exit 1
elif [ ! -d ${POTS_DIR} ]
then
   /bin/mkdir ${POTS_DIR}
fi

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if [ $MERGE = true ]
then
    echo -n "Merge"
else
    echo -n "Overwrite"
fi
echo " existing or create new files from games and webapp GitLab-repos"
echo

echo "Update games and webapp repositories"
(cd ${GAMES_ROOT} && git pull -v)
(cd ${WEBAPP_ROOT} && git pull -v)
echo

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Extracting strings from games (*.ts and *.txt files):"

for i in $(find $GAMES_PATH -mindepth 1 -maxdepth 1 -type d -printf "%f\n" | sort)
do
    DOMAIN=${i}
    echo -n " ${DOMAIN}"
    OPT_PACK_SPECIFIC="--package-name=${DOMAIN} --package-version=${VERSION}"
    INPUT_PATH="${GAMES_PATH}/${DOMAIN}/src"
    INPUT_FILES="*.ts"
    OPT_OUTPUT_DIR="--output-dir=${POTS_DIR}/${DOMAIN}"

    if [ ! -d ${POTS_DIR}/${DOMAIN} ]
    then
	/bin/mkdir ${POTS_DIR}/${DOMAIN}
    fi

    if [ -f ${POTS_DIR}/${DOMAIN}/${TEMPLATE} -a $MERGE = true ]
    then
	OPT_JOIN_EXISTING="--join-existing"
    else
	OPT_JOIN_EXISTING=""
    fi

    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_LANG_SPECIFIC $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_PATH/$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE

    INPUT_FILES="*.txt"
    OPT_JOIN_EXISTING="--join-existing"

    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_ALL $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_PATH/$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE

    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_LANG_SPECIFIC $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_PATH/$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE

done
echo

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Extracting strings from frame, libs and subfolders (selected *.js files)"

DOMAIN="frame"
OPT_PACK_SPECIFIC="--package-name=${DOMAIN} --package-version=${VERSION}"
OPT_OUTPUT_DIR="--output-dir=${POTS_DIR}/${DOMAIN}"
INPUT_FILES=$(find ${FRAME_PATH} -name index.js -o -name messageAPI.js)

if [ ! -d ${POTS_DIR}/${DOMAIN} ]
then
    /bin/mkdir ${POTS_DIR}/${DOMAIN}
fi

if [ -f ${POTS_DIR}/${DOMAIN}/${TEMPLATE} -a $MERGE = true ]
then
    OPT_JOIN_EXISTING="--join-existing"
else
    OPT_JOIN_EXISTING=""
fi

for i in ${INPUT_FILES}
do
    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_LANG_SPECIFIC $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE
done

OPT_JOIN_EXISTING="--join-existing"
INPUT_FILES=$(find ${LIBS_PATH} -name "*.js")

for i in ${INPUT_FILES}
do
    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_LANG_SPECIFIC $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE
done

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Extracting game name strings from game folder names"

DOMAIN="games"
OPT_PACK_SPECIFIC="--package-name=${DOMAIN} --package-version=${VERSION}"
OPT_OUTPUT_DIR="--output-dir=${POTS_DIR}/${DOMAIN}"

OPT_INPUT_LANG="--language=Shell --from-code=UTF-8"
OPT_INPUT_FILE="-"

if [ ! -d ${POTS_DIR}/${DOMAIN} ]
then
    /bin/mkdir ${POTS_DIR}/${DOMAIN}
fi

if [ -f ${POTS_DIR}/${DOMAIN}/${TEMPLATE} -a $MERGE = true ]
then
    OPT_JOIN_EXISTING="--join-existing"
else
    OPT_JOIN_EXISTING=""
fi

find ${GAMES_PATH} -mindepth 1 -maxdepth 1 -type d -printf "\"%f\"\n" | sort \
    | /usr/bin/xgettext \
	  $OPT_INPUT_LANG $OPT_ALL $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	  $OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	  $OPT_INPUT_FILE $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo "Extracting strings from webapp (*.elm and www/*.js files)"

DOMAIN="webapp"
OPT_PACK_SPECIFIC="--package-name=${DOMAIN} --package-version=${VERSION}"
OPT_OUTPUT_DIR="--output-dir=${POTS_DIR}/${DOMAIN}"

OPT_INPUT_LANG="--language=Shell --from-code=UTF-8"
OPT_INPUT_FILE="-"

if [ ! -d ${POTS_DIR}/${DOMAIN} ]
then
    /bin/mkdir ${POTS_DIR}/${DOMAIN}
fi

if [ -f ${POTS_DIR}/${DOMAIN}/${TEMPLATE} -a $MERGE = true ]
then
    OPT_JOIN_EXISTING="--join-existing"
else
    OPT_JOIN_EXISTING=""
fi

${WEBAPP_PATH}/i18n.sh \
    | /usr/bin/xgettext \
	  $OPT_INPUT_LANG $OPT_ALL $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	  $OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	  $OPT_INPUT_FILE $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE

OPT_INPUT_LANG="--language=JavaScript --from-code=UTF-8"
OPT_JOIN_EXISTING="--join-existing"
INPUT_FILES=$(find ${STATS_PATH} -name "*.js")

for i in ${INPUT_FILES}
do
    /usr/bin/xgettext \
	$OPT_INPUT_LANG $OPT_LANG_SPECIFIC $OPT_OUTPUT_DETAILS $OPT_REPORT_ADDRESS \
	$OPT_PACK_SPECIFIC $OPT_JOIN_EXISTING \
	$INPUT_FILES $OPT_OUTPUT_DIR $OPT_OUTPUT_FILE
done

exit 0
